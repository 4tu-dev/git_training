# Git練習リポジトリです。
ここでは、バグが見つかったPSPを修理するフローにたとえてGitの最低限の使い方をかきます。

## 1. 最新版をダウンロードする
バグが見つかった最新版のPSPを手元に持ってきます。

`git pull` コマンドを使用します。最新でないものに修正を加えると、おかしくなってしまうので注意です。

```
$ git pull
```

## 2. ブランチを作成し、切り替える
今からDSを修理しますが、もとのPSPとの区別がつくように、別の名前をつけます。PSP2みたいな。

`git branch` コマンドを使用してブランチ（枝別れ）を作成します。このとき、ブランチ名(-bの後)は、課題のキーにしてください。(e.g. GENERAL-22, SHIKA-9)これにより、この修正は何の課題の修正なのか一目でわかるようになります。 `git checkout` コマンドを使用して作成したブランチに移動します。

```
$ git branch GENERAL-1
$ git checkout GENERAL-1
```

## 3. 修正する
DSを修理します。イイ感じに修理しましょう。

`hello.txt` を開き、 `Tsujimoto` の部分を自分の名前に修正します。コマンドは使用しません。

```
Hello World!
My Name is SakamoTO~!
```

## 4. ラッピングする
`git status` コマンドを使用すると、変更したファイルが赤い文字で表示されます。これは、部品が変わったことはわかるけど、PSPの修理が完了したことをまだgitくんがわかっていない状態です。

```
$ git status
On branch GENERAL-1
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        hello.txt

nothing added to commit but untracked files present (use "git add" to track)
```

`git add` コマンドを使って、どの部品が悪くてどのように修正したかを教えてあげます。

```
$ git add ./hello.txt
```

`git status` コマンドを再度使用すると、変更したファイルが緑色の文字で表示されます。gitくんが、部品の変更を理解しました。

```
$ git status
On branch GENERAL-1
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   hello.txt
```

## 5. 修正をコミットする
バグを直したPSP2をどのように直したのか今回は人にわかるように手紙をつけます。

`git commit` コマンドを使います。 `-m` のあとにコメントをつけます。コメントのつけ方についてはいつか話します。

```
$ git commit -m "fix(general): hello.txtを修正" -m "https://4tu.atlassian.net/browse/GENERAL-1"
```

## 6. プッシュする
修正版PSP2をみんなに教えてあげましょう。

`git push`コマンドを使います。このとき一番最後は必ず2で作ったブランチ名にする必要があります。これで、コマンド打ったりソースコード修正するのは終了です。

```
$ git push origin GENERAL-1
```

## 7. プルリクエストを作成する
今回の修正は本当に正しいのか、ほかの人にチェックしてもらう必要があります。
リポジトリのプルリクエストタブで、プルリクエストを作成しましょう。
https://bitbucket.org/4tu-dev/git_training/pull-requests/

こんな感じにします。 https://bitbucket.org/4tu-dev/git_training/pull-requests/1

## 8. masterにマージする
正しいことがわかれば、最新版として世に出すことができます。これで修正フロー完了です。だいたい辻本がマージします。